package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

public class Model {
	public void getMethod() throws IOException {
		URL url= new URL("https://jsonplaceholder.typicode.com/posts");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		
		con.setConnectTimeout(20000);
		con.setReadTimeout(20000);
		con.setUseCaches(true);
		con.setRequestMethod("GET");
		
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		StringBuilder stringBuilder = new StringBuilder();
		String inputLine;
		while((inputLine = in.readLine()) != null) {
			stringBuilder.append(inputLine);
			
		}
		JSONObject oJSON = new JSONObject(stringBuilder.toString());
		
	    assertNotNull(oJSON);
	    assertEquals("John", json.get("name"));
	    assertEquals(18, json.get("age"));
		
		System.out.println(oJSON);
		in.close();
	}
}
