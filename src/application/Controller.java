package application;

import model.Model;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

public class Controller implements Initializable{
	
	@FXML
	private Button init;
	
	public void initFct(ActionEvent e) {
		try {
			Model model = new Model();
			model.getMethod();
		}catch(IOException e1) {
			System.err.println(e1.getMessage());
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			
			String path = "C:\\Users\\Eleve\\Documents\\Java\\myBD.mdb";
			
			String url = "jdbc:ucanaccess://"+path;
			
			Connection conn = DriverManager.getConnection(url);
		
			Statement statement = conn.createStatement();
			
			ResultSet resultset = statement.executeQuery("select * from myBD");
			
			while(resultset.next()) {
				String id = resultset.getString(1);
				String title = resultset.getString(2);
				String body = resultset.getString(3);
				
				System.out.println(id + ", " + title + ", " + body);
			}
					
			System.out.println("Connection Successfull");
		}catch(Exception e){
			System.err.println("Erreur de connexion");
			System.err.println(e.getMessage());
		}
	}
}
